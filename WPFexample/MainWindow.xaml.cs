﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFexampleEF;
using System.Collections.ObjectModel;

namespace WPFexample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<string> allmyTasks = new ObservableCollection<string>();

        public MainWindow()
        {
            InitializeComponent();
            InitializeListView();
        }

        private void InitializeListView()
        {
            foreach (var item in Program.GetAllTasks())
            {
                allmyTasks.Add(item);
            }

            myTasks.ItemsSource = allmyTasks;
        }

        private void addTask(object sender, RoutedEventArgs e)
        {
            allmyTasks.Add(taskToAdd.Text);

            Program.AddTaskInEF(taskToAdd.Text);
        }

        private void resetTasks(object sender, RoutedEventArgs e)
        {
            var removeAllItems = allmyTasks.ToList();
            foreach (var item in removeAllItems)
            {
                allmyTasks.Remove(item);
            }

            Program.ResetTasksInEF();
        }
    }
}

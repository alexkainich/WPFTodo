﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFexampleEF
{
    public static class Program
    {
        static void Main(string[] args)
        {
        }

        public static void ResetTasksInEF()
        {
            using (var db = new Model1())
            {
                db.WPFtasks.RemoveRange(db.WPFtasks.ToList());

                db.SaveChanges();
            }
        }

        public static void AddTaskInEF(string taskDesc)
        {
            using (var db = new Model1())
            {
                db.WPFtasks.Add(new WPFtask { taskid = Guid.NewGuid().ToString(), task = taskDesc });

                db.SaveChanges();
            }
        }

        public static List<string> GetAllTasks()
        {
            using (var db = new Model1())
            {
                var temp = db.WPFtasks.ToList();
                List<string> result = new List<string>();

                foreach (var item in temp)
                {
                    result.Add(item.task);
                }

                return result;
            }
        }
    }
}
